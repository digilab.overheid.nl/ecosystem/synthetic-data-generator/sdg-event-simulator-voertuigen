
####################################################################################################
## Builder
####################################################################################################

# Pinned to 1.71.1 because the distroless/cc docker image does not support the latest glic version yet
FROM rust:1.71.1 AS builder

RUN update-ca-certificates

# Create appuser
ENV USER=appuser
ENV UID=10001

RUN adduser \
    --disabled-password \
    --gecos "" \
    --home "/nonexistent" \
    --shell "/sbin/nologin" \
    --no-create-home \
    --uid "${UID}" \
    "${USER}"

WORKDIR /app

COPY ./ .

RUN cargo build --release --bin sdg_event_simulator_voertuigen

####################################################################################################
## Final image
####################################################################################################
FROM gcr.io/distroless/cc

# Import from builder.
COPY --from=builder /etc/passwd /etc/passwd
COPY --from=builder /etc/group /etc/group

WORKDIR /app

# Copy our build
COPY --from=builder "/app/target/release/sdg_event_simulator_voertuigen" ./

# Use an unprivileged user.
USER appuser:appuser

CMD [ "/app/sdg_event_simulator_voertuigen" ]