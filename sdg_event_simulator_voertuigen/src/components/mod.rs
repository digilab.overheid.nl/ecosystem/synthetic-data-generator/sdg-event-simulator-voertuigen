use bevy::prelude::*;
use bevy_turborand::prelude::*;

mod age;
mod id;
mod owner;
mod vehicle;

pub use age::Age;
pub use id::Id;
pub use owner::HasVehicle;
pub use owner::Owner;
pub use vehicle::Vehicle;

use crate::data::car_models::CAR_MODELS;

#[derive(Bundle)]
pub struct VehicleBundle {
    pub vehicle: Vehicle,
}

impl VehicleBundle {
    pub fn new(global_rng: &mut ResMut<GlobalRng>) -> Self {
        let mut rng = RngComponent::from(global_rng);
        let id = Id::new(&mut rng);

        let model = CAR_MODELS[rng.usize(0..CAR_MODELS.len())];

        let number_plate = format!(
            "{}-{}{}{}-{}{}",
            rng.uppercase(),
            rng.char('0'..='9'),
            rng.char('0'..='9'),
            rng.char('0'..='9'),
            rng.uppercase(),
            rng.uppercase()
        );

        let vehicle = Vehicle::new(id, model.into(), number_plate);

        Self { vehicle }
    }
}
