use bevy::prelude::Component;
use chrono::NaiveDateTime;

#[derive(Component)]
pub struct Age {
    pub birth_date: NaiveDateTime,
}

impl Age {
    pub fn new(birth_date: NaiveDateTime) -> Self {
        Self { birth_date }
    }
}
