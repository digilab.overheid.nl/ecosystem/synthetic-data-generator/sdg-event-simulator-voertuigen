use bevy::prelude::Component;
use bevy_turborand::{DelegatedRng, RngComponent};
use chrono::Duration;
use sdg_event_simulator_common::{bevy::resources::clock_state::ClockState, chance::PercentageChance};
use super::Id;

#[derive(Component)]
pub struct Vehicle {
    pub id: Id,
    pub model: String,
    pub number_plate: String,
}

impl Vehicle {
    pub fn new(id: Id, model: String, number_plate: String) -> Self {
        Self {
            id, 
            model,
            number_plate,
        }
    }

    pub fn event_occurs(rng: &mut RngComponent, time: &ClockState) -> bool {
        let rand = rng.u64(0..u64::MAX);
        let chance = PercentageChance::new(daily_occurs_chance())
            .scale(Duration::hours(24), time.time_per_tick)
            .fraction_of_u64();

        rand < chance
    }
}

fn daily_occurs_chance() -> f64 {
    0.001
}
