use bevy::prelude::Component;
use bevy_turborand::{RngComponent, DelegatedRng};
use chrono::Duration;
use sdg_event_simulator_common::{chance::PercentageChance, bevy::resources::clock_state::ClockState};

#[derive(Component)]
pub struct HasVehicle;

pub struct Owner;

impl Owner {
    pub fn event_occurs(rng: &mut RngComponent, time: &ClockState) -> bool {
        let rand = rng.u64(0..u64::MAX);
        let chance = PercentageChance::new(daily_occurs_chance())
            .scale(Duration::hours(24), time.time_per_tick)
            .fraction_of_u64();

        rand < chance
    }
}

fn daily_occurs_chance() -> f64 {
    0.00637
}
