use bevy_turborand::{DelegatedRng, RngComponent};
use chrono::NaiveDateTime;
use sdg_event_simulator_common::{config::SdgConfig, event::Event};
use serde::{Deserialize, Serialize};
use serde_json::Value;

use crate::components::{Vehicle, Id};

pub const BIRTH_TOPIC: &str = "events.frp.persoon.GeboorteVastgesteld";
pub const REGISTERED_TOPIC: &str = "events.frv.voertuig.Geregistreerd";
pub const VEHICLE_OWNER_DETERMINED: &str = "events.frv.voertuig.EigenaarVastgesteld";

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct EventVehicleRegistered {
    pub model: String,
    #[serde(rename = "numberPlate")]
    pub number_plate: String,
}

#[derive(Serialize, Deserialize, Clone)]
pub struct EventVehicleOwnerDetermined {
    pub owner: uuid::Uuid,
}

pub fn vehicle_registration_event(
    rng: &mut RngComponent,
    now: &NaiveDateTime,
    vehicle: &Vehicle,
) -> Value {
    let mut bytes = [0; 16];
    rng.fill_bytes(&mut bytes);
    let stable_uuid = uuid::Builder::from_bytes(bytes).into_uuid();

    let data = EventVehicleRegistered {
        model: vehicle.model.clone(),
        number_plate: vehicle.number_plate.clone(),
    };

    let event = Event {
        id: stable_uuid,
        occured_at: now.format(SdgConfig::TIMEFORMAT).to_string(),
        registered_at: now.format(SdgConfig::TIMEFORMAT).to_string(),
        subject_ids: vec![vehicle.id.uuid],
        event_type: "Geregistreerd".into(),
        event_data: serde_json::to_value(data).unwrap(),
    };

    serde_json::to_value(event).unwrap()
}

pub fn vehicle_owner_determined_event(
    rng: &mut RngComponent,
    now: &NaiveDateTime,
    vehicle: &Id,
    owner: &Id,
) -> Value {
    let mut bytes = [0; 16];
    rng.fill_bytes(&mut bytes);
    let stable_uuid = uuid::Builder::from_bytes(bytes).into_uuid();

    let data = EventVehicleOwnerDetermined {
        owner: owner.uuid,
    };

    let event = Event {
        id: stable_uuid,
        occured_at: now.format(SdgConfig::TIMEFORMAT).to_string(),
        registered_at: now.format(SdgConfig::TIMEFORMAT).to_string(),
        subject_ids: vec![vehicle.uuid],
        event_type: "Geregistreerd".into(),
        event_data: serde_json::to_value(data).unwrap(),
    };

    serde_json::to_value(event).unwrap()
}