use bevy::prelude::{Commands, Entity, Query, Res, ResMut};
use bevy_turborand::{GlobalRng, RngComponent};
use sdg_event_simulator_common::bevy::resources::{clock_state::ClockState, nats_state::NatsState};

use crate::{
    components::{Vehicle, VehicleBundle},
    event::{vehicle_registration_event, REGISTERED_TOPIC},
};

pub fn sim_vehicle_registration(
    mut commands: Commands,
    query: Query<(&Vehicle, Entity)>,
    mut global_rng: ResMut<GlobalRng>,
    clock_state: Res<ClockState>,
    nats: Res<NatsState>,
) {
    let mut rng = RngComponent::from(&mut global_rng);

    for _ in query.iter() {
        if !Vehicle::event_occurs(&mut rng, &clock_state) {
            continue;
        }

        let vehicle = VehicleBundle::new(&mut global_rng);

        let message =
            vehicle_registration_event(&mut rng, &clock_state.get_time(), &vehicle.vehicle);

        nats.connection
            .publish(REGISTERED_TOPIC, message.to_string())
            .unwrap();

        commands.spawn(vehicle);
    }
}
