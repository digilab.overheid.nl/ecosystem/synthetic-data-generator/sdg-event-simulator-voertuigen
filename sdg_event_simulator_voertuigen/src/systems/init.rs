use bevy::prelude::*;
use chrono::NaiveDateTime;

use crate::{
    components::{Age, Id, Vehicle},
    event::{EventVehicleRegistered, BIRTH_TOPIC, REGISTERED_TOPIC},
};
use nats::jetstream::{
    self, BatchOptions, ConsumerConfig, JetStream, JetStreamOptions, PullSubscribeOptions,
    PullSubscription,
};
use sdg_event_simulator_common::{bevy::resources::nats_state::NatsState, event::Event};

#[derive(Component)]
pub struct FromSeed;

pub fn get_seed_stream(nats_uri: &str, topic: &str) -> PullSubscription {
    let connection = nats::connect(nats_uri).expect("Could not connect to nats");
    let options = JetStreamOptions::new();
    let stream = JetStream::new(connection, options);

    stream
        .pull_subscribe_with_options(
            topic,
            &PullSubscribeOptions::new().consumer_config(ConsumerConfig {
                deliver_policy: jetstream::DeliverPolicy::All,
                replay_policy: jetstream::ReplayPolicy::Instant,
                filter_subject: topic.to_string(),
                ..Default::default()
            }),
        )
        .unwrap()
}

pub fn load_seed_vehicles(mut commands: Commands, nats_state: Res<NatsState>) {
    info!("Load `seed data vehicles` start");

    let stream = get_seed_stream(&nats_state.uri, REGISTERED_TOPIC);

    loop {
        let messages = stream.fetch(BatchOptions {
            batch: 100,
            expires: None,
            no_wait: true,
        });

        let messages = match messages {
            Ok(m) => m,
            Err(e) => {
                error!("Error loading events batch {}", e);
                break;
            }
        };

        let mut message_count = 0;
        for message in messages {
            // Parse data
            let data = std::str::from_utf8(&message.data).unwrap();
            let event: Event = serde_json::from_str(data).unwrap();
            let event_data: EventVehicleRegistered =
                serde_json::from_value(event.event_data).unwrap();
            let subject = event.subject_ids[0];

            let vehicle = Vehicle::new(
                Id { uuid: subject },
                event_data.model,
                event_data.number_plate,
            );
            commands.spawn((vehicle, FromSeed));

            // Finish
            message.ack().unwrap();
            message_count += 1;
        }

        if message_count == 0 {
            info!("Load `seed data birth` end");
            break;
        }
    }
}

pub fn load_seed_births(mut commands: Commands, nats_state: Res<NatsState>) {
    let stream = get_seed_stream(&nats_state.uri, BIRTH_TOPIC);

    loop {
        let messages = stream.fetch(BatchOptions {
            batch: 100,
            expires: None,
            no_wait: true,
        });

        let messages = match messages {
            Ok(m) => m,
            Err(e) => {
                error!("Error loading events batch {}", e);
                break;
            }
        };

        let mut message_count = 0;
        for message in messages {
            let data = std::str::from_utf8(&message.data).unwrap();
            let event: Event = serde_json::from_str(data).unwrap();
            let birth_date =
                NaiveDateTime::parse_from_str(&event.occured_at, "%Y-%m-%dT%H:%M:%S%.fZ").unwrap();

            commands.spawn((
                Id {
                    uuid: event.subject_ids[0],
                },
                Age::new(birth_date),
            ));

            message.ack().unwrap();
            message_count += 1;
        }

        if message_count == 0 {
            info!("Load `seed data birth` end");
            break;
        }
    }
}

pub fn inspect_seeded(mut query: Query<&Vehicle>) {
    info!("inspect_seeded");

    let count = query.iter().count();
    for vehicle in query.iter_mut() {
        debug!("Found: {}", vehicle.id.to_string());
    }

    info!("inspect_seeded: total found {}", count);
}
