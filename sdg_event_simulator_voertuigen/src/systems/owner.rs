use bevy::prelude::{Commands, Entity, Query, Res, ResMut};
use bevy_turborand::{GlobalRng, RngComponent};
use sdg_event_simulator_common::bevy::resources::{clock_state::ClockState, nats_state::NatsState};

use crate::{
    components::{Age, HasVehicle, Id, Owner},
    event::{vehicle_owner_determined_event, VEHICLE_OWNER_DETERMINED},
};

pub fn sim_vehicle_owner_determined(
    mut commands: Commands,
    query: Query<(&Id, &Age, Entity)>,
    mut global_rng: ResMut<GlobalRng>,
    clock_state: Res<ClockState>,
    nats: Res<NatsState>,
) {
    let mut rng = RngComponent::from(&mut global_rng);

    for (id, _age, entity) in query.iter() {
        if !Owner::event_occurs(&mut rng, &clock_state) {
            continue;
        }
        println!("OWNER determined");

        let message = vehicle_owner_determined_event(&mut rng, &clock_state.get_time(), id, id);

        nats.connection
            .publish(VEHICLE_OWNER_DETERMINED, message.to_string())
            .unwrap();

        commands.entity(entity).insert(HasVehicle);
    }
}
