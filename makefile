SDG_NATS_SERVICE_HOST=0.0.0.0
SDG_NATS_SERVICE_PORT=4222
SLEEP="y"

# Develop apps
.PHONY: run
run:
	SDG_NATS_SERVICE_HOST="$(SDG_NATS_SERVICE_HOST)" SDG_NATS_SERVICE_PORT="$(SDG_NATS_SERVICE_PORT)" SLEEP=$(SLEEP) cargo run --bin sdg_event_simulator_voertuigen

.PHONY: stop
stop:
	pkill -f target/debug/sdg_event_simulator_voertuigen

.PHONY: lint
lint:
	cargo clippy

.PHONY: test
test:
	cargo test --all

# Nats
.PHONY: nats
nats:
	docker run --rm -it --net=host docker.io/library/nats --js

.PHONY: nats_admin
nats_admin:
	docker run --rm -it --net=host --entrypoint=/bin/sh docker.io/natsio/nats-box

# Build / Build test
.PHONY: build_docker
build_docker:
	docker build -f Dockerfile --tag sdg_sim:latest .

.PHONY: run_docker
run_docker:
	docker run \
	-it \
	--rm \
	--net=host \
	--name="sdg_sim" \
	-e SLEEP='y' \
	-e SDG_NATS_SERVICE_HOST='$(SDG_NATS_SERVICE_HOST)' \
	-e SDG_NATS_SERVICE_PORT='$(SDG_NATS_SERVICE_PORT)' \
	sdg_sim:latest